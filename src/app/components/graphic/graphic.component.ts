import { Component, OnInit, OnDestroy } from '@angular/core';
import { AmChartsService, AmChart } from '@amcharts/amcharts3-angular';
import * as _ from 'lodash';

@Component({
  selector: 'app-graphic',
  templateUrl: './graphic.component.html',
  styleUrls: ['./graphic.component.scss']
})
export class GraphicComponent implements OnInit, OnDestroy {

  public options: any;
  public totalCostAlana: number;
  public totalCostNoAlana: number;
  public graphicData : any;

  constructor(private AmCharts: AmChartsService){}


  makeRandomData(){
    const data = [];
    for (let year = 1; year <= 10; ++year) {
      let values = Math.floor(Math.random() * 100)
      data.push({
        year: year,
        alana: values/*,
        noalana : values+(Math.floor(Math.random() * 100))*2*/
      });
    }
    return data;
  }

  makeOptions(dataProvider) {
    return {
      'type': 'serial',
      'theme': 'light',
      'marginTop': 10,
      'hideCredits':true,
      'marginRight': 80,
      'dataProvider': dataProvider,
      'valueAxes': [{
        'axisAlpha': 0,
        'position': 'left',
        'gridThickness': 1.2,
        'color':'#D4D2D2'
      }],
      'graphs': [{
        'title': 'Con alana',
        'id': 'g1',
        'balloonText': '[[category]]<br><b>$<span style=\'font-size:9px;\'>[[value]]</span></b>',
        'lineColor': '#FF7676',
        'lineThickness': 4,
        'type': 'smoothedLine',
        'valueField': 'alana',
        'dashLength' : 0
      }/*,
      {
        'title': 'Sin alana',
        'id': 'g2',
        'balloonText': '[[category]]<br><b>$<span style=\'font-size:9px;\'>[[value]]</span></b>',
        'lineColor': '#CDCDCD',
        'lineThickness': 2,
        'type': 'smoothedLine',
        'valueField': 'noalana',
        'dashLength' : 7
      }*/],
      'chartCursor': {
        'categoryBalloonDateFormat': 'YYYY',
        'cursorAlpha': 0,
        'valueLineEnabled': false,
        'valueLineBalloonEnabled': false,
        'valueLineAlpha': 1,
        'fullWidth': true
      },
      'dataDateFormat': 'YYYY',
      'categoryField': 'year',
      'categoryAxis': {
        'minPeriod': 'YYYY',
        'parseDates': false,
        'minorGridAlpha': 0.1,
        'minorGridEnabled': true,
        "gridThickness": 0,
        "axisColor": '#D4D2D2',
        "axisThickness": 0,
        'color':'#D4D2D2'
      },
      'export': {
        'enabled': false
      }
    };
  }

  setLeyendsSum(){
    this.totalCostAlana = _.sumBy(this.graphicData, 'alana');
    /*this.totalCostNoAlana = _.sumBy(data, 'noalana');*/
  }

  ngOnInit() {
    this.graphicData = this.makeRandomData();
    this.options = this.makeOptions(this.graphicData);
    this.setLeyendsSum();
  }

  ngOnDestroy(){}

}
